const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function toUrl(url, params) {
  let paramsArr = [];
  if (params) {
    Object.keys(params).forEach(item => {
      paramsArr.push(item + '=' + params[item]);
    })
    if (url.search(/\?/) === -1) {
      url += '?' + paramsArr.join('&');
    } else {
      url += '&' + paramsArr.join('&');
    }
  }
  return url;
}
/**
 * request
 */
function requestByToken(url, data = {}, method = "GET",gologin=true) {
  return new Promise(function (resolve, reject) {
    var token = wx.getStorageSync('token');
    if (!token && gologin) {
      wx.navigateTo({
        url: '/pages/login/login',
      })
      return;
    }
    url = toUrl(url, {
      'token': token
    })
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + wx.getStorageSync('token')
      },
      success: function (res) {
        if (res.data.code == 200) {
          if (res.data.data.token)
          {
            wx.setStorage({
              key: 'token',
              data: res.data.data.token,
            })
          }
          resolve(res.data);
        }
        else {
          reject(res.message);
        }

      },
      fail: function (err) {
        reject(err)
        console.log("failed")
      }
    })
  });
}
function request(url, data = {}, method = "GET") {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
      },
      success: function (res) {
        if (res.data.code == 200) {
          resolve(res.data);
        }
        else {
          reject(res.data.message);
        }

      },
      fail: function (err) {
        reject(err)
      }
    })
  });
}
// login: function login() {
//   var that = this;
//   var token = wx.getStorageSync('token');
//   if (token) {
//     _server2.default.get(_urls2.default.links[0].checktoken, { token: token }).then(function (res) {
//       if (res.code != 0) {
//         that.globalData.token = null;
//         that.login();
//       }
//     });
//     return;
//   } else {
//     wx.login({
//       success: function success(res) {
//         _server2.default.get(_urls2.default.links[0].wxapplogin, { code: res.code }).then(function (res) {
//           if (res.code == 1e4) {
//             that.globalData.userinfo = 1e4;
//             return;
//           }
//           if (res.code != 0) {
//             wx.showConfirm({
//               content: "\u5C0F\u7A0B\u5E8F\u79D8\u94A5\u4FE1\u606F\u672A\u914D\u7F6E\u6216\u8005\u914D\u7F6E\u4E0D\u6B63\u786E\uFF0C\u8BF7\u68C0\u67E5\u540E\u91CD\u8BD5",
//               showCancel: false,
//               confirmColor: '#ffd305',
//               confirmText: "\u6211\u77E5\u9053\u4E86",
//               success: function success(res) { }
//             });
//             return;
//           }
//           wx.setStorageSync('token', res.data.token);
//           wx.setStorage({ key: "__appUserInfo", data: { uid: res.data.uid, token: res.data.token } });
//         });
//       }
//     });
//   }
// }

function showErrorToast(msg) {
  wx.showToast({
    title: msg,
    image: '/static/images/icon_error.png',
    duration: 1500
  })
}




module.exports = {
  formatTime: formatTime,
  requestByToken,
  request,
  showErrorToast,
}

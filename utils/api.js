const ApiRootUrl = 'http://lzg.aismart.top/api/';

var version = '';

module.exports = {
  WxloginUrl: ApiRootUrl + 'wxlogin/',
  LaunchUrl: ApiRootUrl + 'launch/', //类目 
  AboutUrl: ApiRootUrl + 'about/', //关于我们 
  IndexUrl: ApiRootUrl + 'index/', //首页数据接口 
  CategoryUrl: ApiRootUrl + 'categories/', //类目 
  ProductUrl: ApiRootUrl + 'products/', //所有商品
  ProductDetailUrl: ApiRootUrl + 'product/', //商品详情
  SearchRecordUrl: ApiRootUrl + 'searchrecords/', //客户搜索记录
  SearchProductUrl: ApiRootUrl + 'search/', //搜索产品
  GetSkuUrl: ApiRootUrl + 'products/sku/', //获取sku
  GetSkuByIdUrl: ApiRootUrl + 'products/getskubyid/', //根据ID获取sku
  GetProductsByCategoryUrl: ApiRootUrl + 'get_products_bycategory/', //查找类目商品
  GetAddressUrl: ApiRootUrl + 'addresses/',//我的地址
  GetAddressByIdUrl: ApiRootUrl + 'address_by_id/',//我的某个地址
  SetDefaultAddressUrl: ApiRootUrl + 'addresses/setdefault',//设置默认地址
  AddAddressUrl: ApiRootUrl + 'addresses/',//添加地址POST
  UpdateAddressUrl: ApiRootUrl + 'addresses/',//更新地址PUT、addresses/{id}
  DelAddressUrl: ApiRootUrl + 'addresses/',//删除地址DELETE、addresses/{id}
  GetCartItemsUrl: ApiRootUrl + 'cart/', //购物车
  AddCartItemsUrl: ApiRootUrl + 'cart/add', //购物车商品add
  IncCartItemsUrl: ApiRootUrl + 'cart/increase', //购物车商品增加
  DesCartItemsUrl: ApiRootUrl + 'cart/decrease', //购物车商品减少
  RemoveCartItemsUrl: ApiRootUrl + 'cart/', //购物车商品删除
  CreateOrderUrl: ApiRootUrl + 'order/create',//提交订单接口 --get
  RePayOrderUrl: ApiRootUrl + 'order/repay',//重新支付订单 --get
  DelOrderUrl: ApiRootUrl + 'order/',//删除订单 order/{id} --DELETE
  GetOrderUrl: ApiRootUrl + 'order',//单个订单接口 --get
  OrdersUrl: ApiRootUrl + 'orders',//所有订单列表 --get
  ApplyRefundOrdersUrl: ApiRootUrl + 'orders/',//申请退款  orders/{order}/apply_refund --post
  ConfirmOrdersUrl: ApiRootUrl + 'orders/',//确认收货 orders/{order}/received --post
  FavorUrl: ApiRootUrl + 'products/',//添加书藏 products/{product}/favorite  --post
  DisFavorUrl: ApiRootUrl + 'products/',//取消收藏 products/{product}/favorite  --delete
  FavoritesUrl: ApiRootUrl + 'products/favorites/',//收藏列表 --get
  IsFavoredUrl: ApiRootUrl + 'product_favor/',//是否已收藏 /product_favor/{product} --get
}
// pages/bb/test/test.js
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    programInfo:{},
    program_title: '',
    sub_program_title: '',
    profile: '',
    copyright: '',
    showRecommend: false, //是否显示推荐模块
    showNewProduct: false, //是否显示新品模块
    showHotProduct: false,//是否显示热卖商品模块
    showVideo: false, //是否显示视频模块
    newProductModulePic: [],
    products: [], //所有商品
    h_products: [], //热卖商品
    r_products: [], //推荐商品
    n_products: [], //新品商品
    videoSrc:'',//视频链接
    preIndex: 0,
    currentIndex: 0,
    animationData: {},
    animationData2: {},
    banners:[],
    photo_shows:[],
    bg_img:'https://i.loli.net/2019/05/18/5cdf883ad145758476.jpg',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadAction();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // wx.startPullDownRefresh()
    this.loadAction();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    let that =this
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    if (that.data.programInfo.share_pic) {
      return {
        title: that.data.programInfo.share_title,
        path: '/pages/index/index',
        imageUrl: that.data.programInfo.share_pic
      }
    }
    return {
      title: that.data.programInfo.share_title,
      path: '/pages/index/index',
    }
  },
  //=============================================
  //自定义方法
  //=============================================

  // 启动操作
  loadAction: function () {
    let that = this
    app.launchData().then(res => {
      that.setData({
        programInfo: app.globalData.programInfo,
        program_title: app.globalData.programInfo.title,
        sub_program_title: app.globalData.programInfo.sub_title,
        profile: app.globalData.programInfo.profile,
        copyright: app.globalData.copyright,
        bg_img: app.globalData.programInfo.bg_img,
      })
    });
    that.getIndexData()
    that.getProducts()
    that.stretch(350)
    wx.stopPullDownRefresh()
  },

  //首页数据
  getIndexData: function () {
    wx.showLoading({
      title: '加载中...',
    })
    let that = this
    util.request(api.IndexUrl).then(function (res) {
      if (res.status == 'success') {
        that.setData({
          showNewProduct: res.data.if_show_newproduct,
          showRecommend: res.data.if_show_recommend,
          showHotProduct: res.data.if_show_hotproduct,
          showVideo: res.data.if_show_video,
          videoSrc: res.data.video_src,
          banners: res.data.banners,
          photo_shows: res.data.photo_shows,
          newProductModulePic: res.data.newproduct_module_pic,
          newproduct_module_title: res.data.newproduct_module_title,
          recommend_module_title: res.data.recommend_module_title,
          hotproduct_module_title: res.data.hotproduct_module_title,
          video_module_title: res.data.video_module_title,
          photo_shows_module_title: res.data.photo_shows_module_title
        })
        app.globalData.programInfo = res.data.miniprograminfo
        wx.hideLoading();
      } else {
        wx.hideLoading();
        if (typeof (res.data) == 'string') {
          util.showErrorToast(res.data);
        }
        else {
          util.showErrorToast("错误，请重试");
        }
      }
    })
  },
  //获取商品列表
  getProducts: function () {
    wx.showLoading({
      title: '加载中...',
    })
    let that = this
    util.request(api.ProductUrl).then(function (res) {
      wx.hideLoading();
      if (res.status == 'success') {
        that.setData({
          products: res.data.products,
          h_products: res.data.h_products,
          r_products: res.data.r_products,
          n_products: res.data.n_products,
        })
      } else {
        if (typeof (res.data) == 'string') {
          util.showErrorToast(res.data);
        }
        else {
          util.showErrorToast("错误，请重试");
        }
      }
    })
  },

  //推荐商品模块item切换
  change(e) {
    if (e.detail.current == 0) {
      let swiperError = this.data.swiperError
      swiperError += 1
      this.setData({ swiperError: swiperError })
      if (swiperError >= 2) {
        this.setData({ currentIndex: this.data.preIndex, swiperError: 0});//，重置current为正确索引
      }

    } 
    //将开关重置为0
    this.setData({ currentIndex: e.detail.current, preIndex: e.detail.current, swiperError: 0 });
    this.stretch(350)
    this.shrink(300)
  },
  // 推荐商品模块item 收缩
  stretch(h) {
    var animation = wx.createAnimation({
      duration: 1000,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(h).step()
    this.setData({
      animationData: animation.export(),
    })
  },
  //推荐商品模块item 展开
  shrink(h) {
    var animation2 = wx.createAnimation({
      duration: 1000,
      timingFunction: 'ease',
    })
    this.animation2 = animation2
    animation2.height(h).step()
    this.setData({
      animationData2: animation2.export()
    })
  },
  // 新品展示列表
  goNew: function () {
    let list = JSON.stringify(this.data.n_products);
    wx.navigateTo({
      url: '/pages/productList/list?newlist=1',
    })
  },
  // 跳转商品详情
  goDetail(e) {
    let pid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/detail/detail?pid=' + pid,
    })
  },
  //购买按钮
  goBuy:function(e)
  {
    app.checkLogin().then(res=>{
      let pid = e.currentTarget.dataset.id
      wx.navigateTo({
        url: '/pages/detail/detail?pid=' + pid + '&show_gg=true',
      })
    }).catch(res=>{
      wx.showToast({
        title: '登录失败',
      })
    })
  },
  imgHeight: function (e) {
    var winWid = wx.getSystemInfoSync().windowWidth; //获取当前屏幕的宽度
    var imgh = e.detail.height;//图片高度
    var imgw = e.detail.width;//图片宽度
    var swiperH = winWid * imgh / imgw + "px"
    this.setData({
      swiperH: swiperH//设置高度
    })
  },
})
// pages/user/favorite/favorite.js

const util = require('../../../utils/util.js')
const api = require('../../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    favoriteList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.fetFavorites()
  },

  fetFavorites:function()
  {
    let that = this
    util.requestByToken(api.FavoritesUrl).then(res=>{
      if (res.status == 'success') {
        that.setData({
          favoriteList: res.data.products
        })
      }
      else {
        if (typeof (res.data) == 'string') {
          util.showErrorToast(res.data);
        }
        else {
          util.showErrorToast("错误，请重试");
        }
      }
    })
  },
  goodsTap:function(e)
  {
    let pid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/detail/detail?pid=' + pid,
    })
  }
})
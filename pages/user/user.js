// pages/user/user.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    copyright: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      copyright: app.globalData.copyright
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


  afterSale:function(){
    wx.showToast({
      title: '开发中',
    })
  },
  userAddress:function()
  {
    app.checkLogin().then(res => {
      wx.navigateTo({
        url: '/pages/address/address',
      })
    }).catch(res => {
      wx.showToast({
        title: '登录失败',
      })
    })
  },
  about:function()
  {
    wx.navigateTo({
      url: '/pages/about/about',
    })
  },
})
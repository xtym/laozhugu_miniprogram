// pages/login/login.js
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  bindGetUserInfo(e) {
    //console.log(e.detail.userInfo)
    wx.showLoading({
      title: '登录中...',
    })
    app.goLogin().then(res=>{
      wx.hideLoading()
      wx.navigateBack({})
    }).catch(res=>{
      wx.showToast({
        title: '登录失败！',
      })
    })
  }
})
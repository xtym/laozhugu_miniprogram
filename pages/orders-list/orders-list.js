var app = getApp();
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    search:'',
    nowstatus: "",
    orderlist: [],
    // search: "",
    first: 0,
    title: "玩命加载中...",
    hidden: false
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (parseInt(options.nowstatus)) {
      this.setData({
        nowstatus: (parseInt(options.nowstatus) - 1).toString()
      })
      this.getorderlist(parseInt(options.nowstatus) - 1);
    } else {
      this.getorderlist("");
    }
  },
  getorderlist: function (e) {
    var that = this;
    var search = that.data.search;
    var limit = 8;
    var first = that.data.first;
    var startpage = limit * first;
    util.requestByToken(api.OrdersUrl,{
      search :that.data.search,
      limit : 8,
      skip: startpage,
      type:e
    }).then(function (res) {
      if (res.status == 'success') {
        var len = res.data.orders.length;
        if(len >0)
        {
          for (var i = 0; i < len; i++) {
            that.data.orderlist.push(res.data.orders[i]);
          }
          that.setData({
            first: first + 1,
            orderlist: that.data.orderlist
          })
        }
        if (len < limit) {
          that.setData({
            title: "数据已经加载完成",
            hidden: true
          });
          return false;
        }
      } else {
        util.showErrorToast('网络错误');
      }
    })


    // wx.request({
    //   url: app.globalData.url + '/routine/auth_api/get_user_order_list?uid=' + app.globalData.uid,
    //   data: { type: e, search: search, skip: startpage, limit: limit },
    //   method: 'get',
    //   header: header,
    //   success: function (res) {

    //     var $data = res.data.data;
    //     var len = $data.length;
    //     for (var i = 0; i < len; i++) {
    //       that.data.orderlist.push($data[i]);
    //     }
    //     that.setData({
    //       first: first + 1,
    //       orderlist: that.data.orderlist
    //     });
    //     console.log(that.data.orderlist)
    //     if (len < limit) {
    //       that.setData({
    //         title: "数据已经加载完成",
    //         hidden: true
    //       });
    //       return false;
    //     }
    //   },
    //   fail: function (res) {
    //     console.log('submit fail');
    //   },
    //   complete: function (res) {
    //     console.log('submit complete');
    //   }
    // });
  },
  statusClick: function (e) {
    var nowstatus = e.currentTarget.dataset.show;
    this.setData({
      nowstatus: nowstatus,
      orderlist: [],
      first: 0,
      title: "玩命加载中...",
      hidden: false
    });
    this.getorderlist(nowstatus);
  },
  searchSubmit: function () {
    this.setData({
      orderlist: [],
      first: 0,
      title: "玩命加载中...",
      hidden: false
    });
    var e = this.data.nowstatus;
    this.getorderlist(e);
  },
  searchInput: function (e) {
    this.setData({
      search: e.detail.value
    });
  },
  delOrder: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确认删除订单？',
      content: '订单删除后将无法查看',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '删除中...',
            icon:'none'
          })
          let url = api.DelOrderUrl + id
          util.requestByToken(url, {}, 'DELETE').then(function (res) {
            wx.hideLoading()
            if (res.status == 'success') {
              wx.showToast({
                title: '删除成功',
                icon: 'success',
                duration: 1000,
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '/pages/orders-list/orders-list',
                })
              }, 1500)
            } else {
              util.showErrorToast(res.data);
            }
          })
        } else {
          wx.showToast({
            title: '用户取消',
            icon: 'none'
          })
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '用户取消',
          icon: 'none'
        })
      }
    })
  },
  confirmOrder:function(e)
  {
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.showModal({
      title: '确认收货',
      content: '为保障权益，请收到货确认无误后，再确认收货',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '确认收货中...',
          })
          let url = api.ConfirmOrdersUrl + id + '/received'
          util.requestByToken(url, { }, 'post').then(function (res) {
            wx.hideLoading();
            if (res.status == 'success') {
              wx.showToast({
                title: '已确认收货',
                icon: 'none',
              })
              that.getorderlist(that.data.nowstatus);
            } else {
              util.showErrorToast(res.data);
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  //底部加载
  onReachBottom: function () {
    var e = this.data.nowstatus;
    this.getorderlist(e);
  }
})
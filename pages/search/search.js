var app = getApp();
// pages/search/search.js
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
Page({
  data: {
    focus: true,
    hotKeyShow: true,
    historyKeyShow: true,
    searchValue: '',
    page: 0,
    productData: [],
    historyKeyList: [],
    hotKeyList: [],
    recommend_products:{},
  },
  onLoad: function (options) {
    
  },
  onShow:function()
  {
    var that = this;
    util.request(api.SearchRecordUrl).then(function (res) {
      if (res.status == 'success') {
        that.setData({
          historyKeyList: res.data.mylogs,
          hotKeyList: res.data.toplogs,
          recommend_products: res.data.r_products,
        })
      } else {
        util.showErrorToast(res.data);
      }
    })
  },
  onReachBottom: function () {
    //下拉加载更多多...
    this.setData({
      page: (this.data.page + 10)
    })

    this.searchProductData();
  },
  doKeySearch: function (e) {
    var key = e.currentTarget.dataset.key;
    this.setData({
      searchValue: key,
      hotKeyShow: false,
      historyKeyShow: false,
    });

    this.data.productData.length = 0;
    this.searchProductData();
  },
  doSearch: function () {
    var searchKey = this.data.searchValue;
    if (!searchKey) {
      this.setData({
        focus: true,
        hotKeyShow: true,
        historyKeyShow: true,
      });
      util.showErrorToast('请输入关键词');
      return;
    };

    this.setData({
      hotKeyShow: false,
      historyKeyShow: false,
    })

    this.data.productData = {};
    this.searchProductData();

    this.getOrSetSearchHistory(searchKey);
  },
  getOrSetSearchHistory: function (key) {
    var that = this;
    wx.getStorage({
      key: 'historyKeyList',
      success: function (res) {
        console.log(res.data);

        //console.log(res.data.indexOf(key))
        if (res.data.indexOf(key) >= 0) {
          return;
        }

        res.data.push(key);
        wx.setStorage({
          key: "historyKeyList",
          data: res.data,
        });

        that.setData({
          historyKeyList: res.data
        });
      }
    });
  },
  searchValueInput: function (e) {
    var value = e.detail.value;
    this.setData({
      searchValue: value,
    });
    if (!value && this.data.productData.length == 0) {
      this.setData({
        hotKeyShow: true,
        historyKeyShow: true,
      });
    }
  },
  searchProductData: function () {
    var that = this; 
    let url = api.SearchProductUrl
    if (wx.getStorageSync('token') && wx.getStorageSync('userInfo'))
    {
      util.requestByToken(url,
        {
          'keyword': that.data.searchValue
        },
        "POST"
      ).then(function (res) {
        if (res.status == 'success') {
          that.setData({
            productData: res.data.prolist,
          })
        } else {
          util.showErrorToast(res.data);
        }
      })
    }
    util.request(url,
      {
        'keyword': that.data.searchValue
      },
      "POST"
    ).then(function (res) {
      if (res.status == 'success') {
        that.setData({
          productData: res.data.prolist,
        })
      } else {
        util.showErrorToast(res.data);
      }
    })
  },
  // 跳转商品详情
  goDetail(e) {
    let pid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/detail/detail?pid=' + pid,
    })
  },
});
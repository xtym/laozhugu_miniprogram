var app = getApp();
const util = require('../../../utils/util.js')
const api = require('../../../utils/api.js')
Page({
  data: {
    _num: 1,
    region: ['省', '市', '区'],
    cartId: '',
    pinkId: '',
    couponId: '',
    id: 0,
    userAddress: []
  },
  onLoad: function (options) {
    if (options.cartId) {
      this.setData({
        cartId: options.cartId,
        pinkId: options.pinkId,
        couponId: options.couponId,
      })
    }
    if (options.id) {
      this.setData({
        id: options.id
      })
      this.getUserAddress();
    }
  },
  getUserAddress: function () {//get_user_address
    var that = this;

    util.requestByToken(api.GetAddressByIdUrl, { 'id': that.data.id}).then(function (res) {
      wx.hideLoading();
      if (res.status == 'success') {
        var regionOne = "region.0";
        var regionTwo = "region.1";
        var regionTherr = "region.2";
        that.setData({
          userAddress: res.data.address,
          [regionOne]: res.data.address.province,
          [regionTwo]: res.data.address.city,
          [regionTherr]: res.data.address.district,
          _num: res.data.address.is_default
        })
      } else {
        util.showErrorToast(res.data);
      }
    })
  },
  bindRegionChange: function (e) {
    console.log(e.detail.value)
    console.log(JSON.stringify(e.detail.value))
    this.setData({
      region: e.detail.value
    })
  },
  defaulttap: function (e) {
    var num = this.data._num;
    if (num == 1) {
      this.setData({
        _num: 0
      })
    } else {
      this.setData({
        _num: 1
      })
    }
  },

  formSubmit: function (e) {
    var warn = "";
    var that = this;
    var flag = true;
    var cartId = '';
    var name = e.detail.value.name;
    var phone = e.detail.value.phone;
    var area = JSON.stringify(this.data.region);
    var fulladdress = e.detail.value.fulladdress;
    if (name == "") {
      warn = '请输入姓名';
    } else if (!/^1(3|4|5|7|8|9)\d{9}$/i.test(phone)) {
      warn = '您输入的手机号有误'
    } else if (area == '["省","市","区"]') {
      warn = '请选择地区';
    } else if (fulladdress == "") {
      warn = "请填写具体地址";
    } else {
      flag = false;
    }
    if (flag == true) {
      wx.showModal({
        title: '提示',
        content: warn
      })
    } else {
      wx.showToast({
        title: '保存中...',
        icon: 'none'
      })
      util.requestByToken(api.AddAddressUrl, {
        'province': that.data.region[0],
        'city': that.data.region[1],
        'district': that.data.region[2],
        'address': fulladdress,
        'contact_name': name,
        'contact_phone': phone,
        'is_default': that.data._num,
        'id': that.data.id
      }, 'POST').then(function (res) {
        if (res.status == 'success') {
          wx.showToast({
            title: '保存成功',
            icon: 'success',
            duration: 1000
          })
          setTimeout(function () {
            if (that.data.cartId) {
              var cartId = that.data.cartId;
              var pinkId = that.data.pinkId;
              var couponId = that.data.couponId;
              that.setData({
                cartId: '',
                pinkId: '',
                couponId: '',
              })
              wx.navigateTo({ //跳转至指定页面并关闭其他打开的所有页面（这个最好用在返回至首页的的时候）
                url: '/pages/order-confirm/order-confirm?id=' + cartId + '&addressId=' + that.data.id + '&pinkId=' + pinkId + '&couponId=' + couponId
              })
            } else {
              wx.navigateBack({})
            }
          }, 1200)
        } else {
          util.showErrorToast(res.data);
        }
      })
    }
  }


})
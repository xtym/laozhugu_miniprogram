// pages/address/address.js
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
var app = getApp();
Page({
  data: {
    _num: '',
    cartId: '',
    pinkId: '',
    addressArray: []
  },
  onLoad: function (options) {
    if (options.backorder) {
      this.setData({
        // cartId: options.cartId,
        // pinkId: options.pinkId,
        backorder: options.backorder
      })
    }
  },
  onShow() {
    this.getAddress();
  },
  getWxAddress: function () {
    wx.showToast({
      title: '读取中',
      icon: 'none'
    })
    var that = this;
    wx.authorize({
      scope: 'scope.address',
      success: function (res) {
        wx.chooseAddress({
          success: function (res) {
            util.requestByToken(api.AddAddressUrl, {
              'province': res.provinceName,
              'city': res.cityName,
              'district': res.countyName,
              'address': res.detailInfo,
              'contact_name': res.userName,
              'contact_phone': res.telNumber,
              'is_default': 0,
            }, 'POST').then(function (res) {
              if (res.status == 'success') {
                that.getAddress();
              } else {
                util.showErrorToast(res.data);
              }
            })
          },
          fail: function (res) {
            if (res.errMsg == 'chooseAddress:cancel') {
              wx.showToast({
                title: '取消选择',
                icon: 'none',
                duration: 1500
              })
            }
          },
          complete: function (res) { },
        })
      },
      fail: function (res) {
        console.log(res);
      },
      complete: function (res) { },
    })
  },
  getAddress: function () {
    wx.showToast({
      title: '加载中...',
      icon: 'none'
    })
    var that = this;
    util.requestByToken(api.GetAddressUrl).then(function (res) {
      wx.hideLoading();
      if (res.status == 'success') {
        that.setData({
          addressArray: res.data.addresses,
        })
        for (var i in res.data.addresses) {
          if (res.data.addresses[i].is_default == 1) {
            that.setData({
              _num: res.data.addresses[i].id
            })
          }
        }
      } else {
        util.showErrorToast(res.data);
      }
    })
  },
  addAddress: function () {

    var cartId = this.data.cartId;
    var pinkId = this.data.pinkId;
    var couponId = this.data.couponId;
    this.setData({
      cartId: '',
      pinkId: '',
      couponId: '',
    })
    wx.navigateTo({ //跳转至指定页面并关闭其他打开的所有页面（这个最好用在返回至首页的的时候）
      url: '/pages/address/editAddress/index?cartId=' + cartId + '&pinkId=' + pinkId
    })
  },
  goOrder: function (e) {
    var id = e.currentTarget.dataset.id;
    // var cartId = '';
    // var pinkId = '';
    // var couponId = '';
    if (this.data.backorder && id) {
      // cartId = this.data.cartId;
      // pinkId = this.data.pinkId;
      // couponId = this.data.couponId;
      // this.setData({
      //   cartId: '',
      //   pinkId: '',
      //   couponId: '',
      // })
      // wx.navigateTo({ //跳转至指定页面并关闭其他打开的所有页面（这个最好用在返回至首页的的时候）
      //   url: '/pages/orderConfirm/orderConfirm?addressId=' + id 
      // })
      var pages = getCurrentPages();
      var currPage = pages[pages.length - 1];   //当前页面
      var prevPage = pages[pages.length - 2];  //上一个页面
      prevPage.setData({
        addressId: id
      })
      wx.navigateBack({})
    }
  },
  delAddress: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确认删除吗',
      success: function (res) {
        if (res.confirm) {
          let url = api.DelAddressUrl + id
          util.requestByToken(url, {}, 'DELETE').then(function (res) {
            if (res.status == 'success') {
              wx.showToast({
                title: '删除成功',
                icon: 'success',
                duration: 1000,
              })
              that.getAddress();
            } else {
              util.showErrorToast(res.data);
            }
          })
        } else {
          console.log(res);
        }
      },
      fail: function (res) {
        console.log(res);
      }
    })
  },
  editAddress: function (e) {
    var cartId = this.data.cartId;
    var pinkId = this.data.pinkId;
    var couponId = this.data.couponId;
    this.setData({
      cartId: '',
      pinkId: '',
      couponId: '',
    })
    wx.navigateTo({ //跳转至指定页面并关闭其他打开的所有页面（这个最好用在返回至首页的的时候）
      url: '/pages/address/editAddress/index?id=' + e.currentTarget.dataset.id + '&cartId=' + cartId + '&pinkId=' + pinkId + '&couponId=' + couponId
    })
  },
  activetap: function (e) {
    var id = e.target.dataset.idx;
    var that = this;
    wx.showLoading({
      title: '设置中',
    })
    util.requestByToken(api.SetDefaultAddressUrl, { id: id }, 'POST').then(function (res) {
      wx.hideLoading()
      if (res.status == 'success') {
        wx.showToast({
          title: '设置成功',
          icon: 'success',
          duration: 1000,
        })
        that.getAddress();
      } else {
        util.showErrorToast(res.data);
      }
    })
  }
})
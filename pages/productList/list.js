// pages/productList/list.js
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    category_id: "",
    product_list: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    if (options.category) {
      that.setData({
        category_id: options.category
      })
    }
    if (options.newlist) {
      var pages = getCurrentPages();
      var Page = pages[pages.length - 1];//当前页
      var prevPage = pages[pages.length - 2];  //上一个页面
      var plists = prevPage.data.n_products //取上页data里的数据也可以修改
      that.setData({
        product_list: plists
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getProductsByCategory()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //================自定义方法==========================

  //获取列表
  getProductsByCategory: function () {
    let that = this
    if (that.data.category_id) {
      util.request(api.GetProductsByCategoryUrl, {
        "category": that.data.category_id,
      }, "POST").then(function (res) {
        if (res.status == 'success') {
          that.setData({
            product_list: res.data.products
          })
          wx.setNavigationBarTitle({
            title: res.data.category_name
          })
        }
        else {
          if (typeof (res.data) == 'string') {
            util.showErrorToast(res.data);
          }
          else {
            util.showErrorToast("错误，请重试");
          }
        }
      })
    }
  },
  //  商品详情
  goDetail(e) {
    let pid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/detail/detail?pid=' + pid,
    })
  },

})
// pages/orders-con/orders-con.js
var app = getApp();
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order:{},
    order_status:'',
    ship_status:'',
    hiddenmodalput: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    var that = this;
    wx.showToast({
      title: '正在加载中……',
      icon:'none'
    })
    util.requestByToken(api.GetOrderUrl, { id: e.order_id}).then(function (res) {
      wx.hideToast();
      if (res.status == 'success') {
        that.setData({
          order: res.data.order,
        })
        //物流状态
        if (res.data.order.ship_status =='pending')
        {
          that.setData({
            ship_status: '待发货',
          })
        }
        else if (res.data.order.ship_status == 'delivered') {
          that.setData({
            ship_status: '已发货',
          })
        }
        else if (res.data.order.ship_status == 'received') {
          that.setData({
            ship_status: '已收货',
          })
        }
        else{
          that.setData({
            ship_status: '异常',
          })
        }
      } else {
        util.showErrorToast(res.data);
      }
    })
  },
  //选择付款方式
  // checkPay: function (e) {
  //   var that = this;
  // },
  //立即付款
  getPay: function (e) {
    var that = this;
    wx.showToast({
      title: '支付订单...',
      icon:'none'
    })
    util.requestByToken(api.RePayOrderUrl,
      {
        id: that.data.order.id,
      }, 'POST').then(function (res) {
        if (res.status == 'success') {
          var jsConfig = res.data.conf;
          wx.requestPayment({
            timeStamp: jsConfig.timestamp,
            nonceStr: jsConfig.nonceStr,
            package: jsConfig.package,
            signType: jsConfig.signType,
            paySign: jsConfig.paySign,
            success: function (r) {
              wx.showToast({
                title: '支付成功',
                icon: 'success',
                duration: 1000,
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '/pages/orderDetail/orderDetail?order_id=' + res.data.order_id
                })
              }, 1200)
            },
            fail: function (r) {
              wx.showToast({
                title: '支付取消',
                icon: 'none',
                duration: 1000,
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '/pages/orderDetail/orderDetail?order_id=' + res.data.order_id
                })
              }, 1200)
            },
            complete: function (r) {
              if (r.errMsg == 'requestPayment:cancel') {
                wx.showToast({
                  title: '取消支付',
                  icon: 'none',
                  duration: 1000,
                })
                setTimeout(function () {
                  wx.redirectTo({
                    url: '/pages/orderDetail/orderDetail?order_id=' + res.data.order_id
                  })
                }, 1200)
              }
            },
          })
        } else {
          util.showErrorToast(res.data);
        }
      })
  },
  // 删除订单
  delOrder: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确认删除订单？',
      content: '订单删除后将无法查看',
      success: function (res) {
        if (res.confirm) {
          wx.showToast({
            title: '删除中...',
            icon: 'none'
          })
          let url = api.DelOrderUrl + id
          util.requestByToken(url, {}, 'DELETE').then(function (res) {
            wx.hideToast()
            if (res.status == 'success') {
              wx.showToast({
                title: '删除成功',
                icon: 'success',
                duration: 1000,
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '/pages/orders-list/orders-list',
                })
              }, 1500)
            } else {
              util.showErrorToast(res.data);
            }
          })
        } else {
          wx.showToast({
            title: '取消删除',
            icon: 'none'
          })
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '取消删除',
          icon:'none'
        })
      }
    })
  },
  //确认收货
  confirmOrder: function (e) {
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.showModal({
      title: '确认收货',
      content: '为保障权益，请收到货确认无误后，再确认收货',
      success: function (res) {
        if (res.confirm) {
          wx.showToast({
            title: '确认收货中...',
            icon:'none'
          })
          let url = api.ConfirmOrdersUrl + id + '/received'
          util.requestByToken(url, {}, 'post').then(function (res) {
            wx.hideLoading();
            if (res.status == 'success') {
              wx.showToast({
                title: '已确认收货',
                icon: 'none',
              })
              that.setData({
                order: res.data.order,
              })
            } else {
              util.showErrorToast(res.data);
            }
          })
        } else if (res.cancel) {
          console.log('取消收货')
        }
      }
    })
  },
  //取消退款申请
  cancelM: function (e) {
    this.setData({
      hiddenmodalput: true,
    })
  },
  //点击申请退款
  applyrefund:function()
  {
    this.setData({
      hiddenmodalput: false,
    })
  },
  //提交退款申请
  confirmM: function () {
    let that = this
    if (!that.data.reason)
    {
      wx.showToast({
        title: '请输入退款理由',
        icon:'none'
      })
      return false;
    }
    let url = api.ApplyRefundOrdersUrl + that.data.order.id +'/apply_refund'
    util.requestByToken(url, { reason: that.data.reason },'post').then(function (res) {
      wx.hideLoading();
      if (res.status == 'success') {
        wx.showToast({
          title: '申请成功',
          icon:'none',
        })
        that.setData({
          order: res.data.order,
          hiddenmodalput: true,
        })
      } else {
        util.showErrorToast(res.data);
      }
    })
  },
  //退款理由输入框
  reason: function (e) {
    this.setData({
      reason: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
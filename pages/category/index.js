// pages/category/index.js
const util = require('../../utils/util.js')
const api = require('../../utils/api.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categories: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    that.getCategory();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //-----------------------------------------------//
  /**
   * 自定义函数
   */
  //-----------------------------------------------//
  // 获取类目
  getCategory: function () {
    wx.showToast({
      title: '加载中...',
      icon: 'none',
    })
    let that = this
    util.request(api.CategoryUrl).then(function (res) {
      wx.hideToast()
      if(res.status == 'success')
      {
        that.setData({
          categories:res.data.categories
        })
      }
      else{
        if (typeof (res.data) == 'string') {
          util.showErrorToast(res.data);
        }
        else {
          util.showErrorToast("错误，请重试");
        }
      }
    })
  }
})
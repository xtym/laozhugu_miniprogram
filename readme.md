# LShop 老主顾商城小程序端 v0.0.2
### 介绍
目前开源的小程序商城有很多，但真正能上线使用的没几个，就算能上线使用做的也相对比较粗糙。老主顾商城基于真实商业项目后开源，更注重实际应用，在用户视觉和交互方面做了很多改进。 功能方面目前开源的是最基础版本，适用于一般的需求，下一步更多营销功能正在陆续添加中，欢迎Star～

- 后台服务端代码请点击 https://gitee.com/jehaz/laozhugu_service
- 加入交流群请先加好友，备注：老主顾商城
<p align="left">
	<img src="https://i.loli.net/2019/05/18/5ce0085c2cffb21560.jpeg" alt="Sample"  width="150" height="150">
</p>

### 现有功能
##### 目前完成的是满足基础需求的商城。小程序端拥有完整的下单、加购、在线支付、确认收货、取消订单等功能；后台可以拥有完整的新建发布商品、上下架订单功能，用户管理、店铺信息管理、店铺模块管理、商品类目管理功能，用户订单发货、在线退款等功能。满足一般的店铺需求。更多营销及附加功能目前正在开发之中。

### 正在开发

- ~~收藏功能~~ ✔
- 优惠券
### 即将开发

1. ~~优惠券~~
2. 秒杀
3. 分销
4. 拼团
5. 砍价
6. 用户浏览足迹
7. 用户画像

### 软件架构
- 服务器配置：>=php7 (需安装fileinfo扩展)、redis、mysql5.7
- 后端框架：laravel 5.8
- 微信框架：easywechat ~5.0
### 安装

- 克隆项目到本地
- 用微信开发者工具打开，填写你自己的APPID信息，修改api.js文件的url为你自己的api接口（测试接口：https://nmd.surebetter.cn/api）
- 微信开发者工具项目设置里面，把『不校验合法域名、web-view（业务域名）、TLS 版本以及 HTTPS 证书』选项选中，进行预览
- 测试接口对应的演示后台：http://nmd.surebetter.cn     admin/admin
### 案例
![娜美达](https://i.loli.net/2019/05/18/5ce0043ef132357090.jpg)]

<span align="left">
	<img src="https://i.loli.net/2019/05/18/5ce013bfe715497264.jpeg" alt="Sample"  width="180" height="320">
</span>
<span align="left">
	<img src="https://i.loli.net/2019/05/24/5ce6c44ce56e389075.jpeg" alt="Sample"  width="180" height="320">
</span>
<span align="left">
	<img src="https://i.loli.net/2019/05/18/5ce013bfa9d7f45586.jpeg" alt="Sample"  width="180" height="320">
</span>
<span align="left">
	<img src="https://i.loli.net/2019/05/18/5ce013bf8317d12348.jpeg" alt="Sample"  width="180" height="320">
</span>

## 问题反馈

扫描微信二维码，添加 老主顾商城（WeChatID: monco0421） 为联系人 发送消息“老主顾”，即可获取最新的微信群二维码
<p align="left">
	<img src="https://i.loli.net/2019/05/18/5ce0085c2cffb21560.jpeg" alt="Sample"  width="150" height="150">
</p>
如果你在配置过程中遇到问题或者有更好的开发建议 请在群内反馈交流。

## 更新记录
- 【2019-05-20】 v0.0.1 基础版本，产品规格固定颜色和尺寸
- 【2019-05-24】 v0.0.2 自定义规格属性、商品收藏功能

# License

MIT License

Copyright (c) 2018 O2Team

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

你可以免费使用、修改、发布、分发该软件，但需要保留小程序底部的版权信息（logo等字样）。
